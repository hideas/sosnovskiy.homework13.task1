﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework13.Task1
{
    public partial class Form : System.Windows.Forms.Form
    {
        Random random; // Random variable

        // Constructor
        public Form()
        {
            InitializeComponent();

            // Randomizing button position on form
            random = new Random();
            button_RandomLocation();

            // Setting button invisible
            btnToFind.Visible = false;
        }

        // Method to randomize position
        private void button_RandomLocation()
        {
            // Width: 0 - 360, Height: 0 - 360
            int widthDifference = this.ClientRectangle.Width - btnToFind.Width;
            int heightDifference = this.ClientRectangle.Height - btnToFind.Height - statusStrip.Height;
            btnToFind.Location = new Point(random.Next(0, widthDifference), random.Next(0, heightDifference));
        }

        // Method on load form
        private async void Form_Load(object sender, EventArgs e)
        {
            // Setting text blink on form title for 3s
            titleBlinking.Start();
            await Task.Delay(3000);
            titleBlinking.Dispose();

            this.Text = "Cold'n'Hot";
        }

        // Method to make title blinking
        private void title_Blink(object sender, EventArgs e)
        {
            this.Text = this.Text.Equals("Hover over the form to start") ? " " : "Hover over the form to start";
        }

        // Method to test real form width and button location
        private void Form_MouseClick(object sender, MouseEventArgs e)
        {
            // this.Text = $"X: {e.X} Y: {e.Y}";
            // toolStripStatusLabel.Text = $"X: {btnToFind.Location.X} Y: {btnToFind.Location.Y}";
        }

        // Method on mouse move
        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            // Generating class with disnatce to button coordinates 
            CoordinatesDifference coordinates = new CoordinatesDifference
            {
                cursorX = e.X,
                buttonX = btnToFind.Location.X + (btnToFind.Width / 2),
                cursorY = e.Y,
                buttonY = btnToFind.Location.Y + (btnToFind.Height / 2)
            };

            // Setting statuses and button visibility
            coordinates.settingStatuses(toolStripStatusLabel, btnToFind);
        }

        // Method on click button
        private void btnToFind_Click(object sender, EventArgs e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show("You win!", "Restart?", buttons);

            if (result == DialogResult.Yes)
            {
                Application.Restart();
            }
            else
            {
                this.Close();
            }
        }
    }
}
