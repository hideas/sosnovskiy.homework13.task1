﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework13.Task1
{
    class CoordinatesDifference
    {
        public int cursorX { get; set; }
        public int buttonX { get; set; }
        public int cursorY { get; set; }
        public int buttonY { get; set; }

        public void settingStatuses(ToolStripStatusLabel toolStripStatusLabel, Button btnToFind)
        {
            int xDifference = Math.Abs(cursorX - buttonX);
            int yDifference = Math.Abs(cursorY - buttonY);

            int sumCoordinates = Math.Abs(xDifference + yDifference);

            if (sumCoordinates > 120)
            {
                toolStripStatusLabel.Text = "Cold";
                toolStripStatusLabel.ForeColor = Color.FromArgb(20, 0, 255);
            }
            if (sumCoordinates > 60 && sumCoordinates < 120)
            {
                toolStripStatusLabel.Text = "Warmer";
                toolStripStatusLabel.ForeColor = Color.FromArgb(20, 130, 200);
            }
            if (sumCoordinates > 30 && sumCoordinates < 60)
            {
                toolStripStatusLabel.Text = "Warm";
                toolStripStatusLabel.ForeColor = Color.FromArgb(195, 190, 40);
            }
            if (sumCoordinates > 20 && sumCoordinates < 30)
            {
                toolStripStatusLabel.Text = "Hot";
                toolStripStatusLabel.ForeColor = Color.FromArgb(255, 170, 50);
            }
            if (sumCoordinates < 20)
            {
                toolStripStatusLabel.Text = "Burn";
                toolStripStatusLabel.ForeColor = Color.FromArgb(255, 0, 0);
            }
            if (sumCoordinates < 10)
            {
                btnToFind.Visible = true;
            }
        }
    }
}
